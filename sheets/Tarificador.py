def Tarificador():
    import pandas as pd
    import pygsheets
    import cx_Oracle
    import datetime
    #import os
    
    print("Cargando paquetes a las: " + str(datetime.datetime.now()))
    #os.chdir('app')
    
    def VaciosaNone(df):
    # Funcion para buscar nan y nat dentro de un dataframe y convertirlos en vacios
    # Cambia los nan y NaT a Vacios, no sirve el pd.where para cambiar los NaT a Vacios
        df = df.apply(lambda x: x.apply(lambda y: '' if y != y else y ),axis=0)
        df = df.where((pd.notnull(df)),'')
        return(df)
    
    
    def changedtype(df, columns):
        for i in columns:
            df[i]=df[i].astype(str)
            df[i]=df[i].str.replace('.', ',')
        return(df)
    
    print('cargando tarificador chile')
    print("Cargando base a las: " + str(datetime.datetime.now()))
    con = cx_Oracle.connect('PGC_CL/PGC_CL@//Oracle.avalchile.cl:1521/ORCL')
    query = """select q.*
    
     , case when (q.tasa_tarifario is null or q.tasa_tarifario = 0) then 'NO APLICA'
                when q."Tasa_Real" >= q.tasa_tarifario then 'CUMPLE'
                else  'NO CUMPLE' end as "Cumplimiento"
            
     ,  case when q.tasa_tarifario>0 then round(((q."Tasa_Real" - q.tasa_tarifario)/q.tasa_tarifario)*100,2)
          else null end as PORC_DESVIACION
    
    
    from
        (Select PD.ID_POLIZA                                                                           as   "Poliza"
           , PO.ID_PROPUESTA                                                                        as   "Propuesta"
          -- , NVL(PD.NUM_ENDOSO, 0)                                                                  as   "Endoso" 
           , COB.DESCRIP_DATO                                                                       as   "Cobertura" 
           , TRUNC(PD.FEC_INICIAL)                                                                  as   "Fecha Inicio"
           , TRUNC(PD.FEC_FINAL)                                                                    as   "Fecha Termino"
           ,to_char(pd.fec_registra,'yyyy-mm')                                                      as "Fecha Emision"
       --    , TRUNC(PD.FEC_REGISTRA)                                                                 as   "Fecha Emision"
           , case when MO.DSTIPO_MONEDA = 'DOLARES' then 'US$'
             else MO.DSTIPO_MONEDA
             end                                                                                    as   "Moneda"
           , PD.VAL_ASEGURADO_DIFERENCIAL                                                           as   "Capital" 
           , round(PD.PORCENTAJE_TASA,2)                                                                     as   "Tasa"
           , PD.VAL_PRIMA_DIFERENCIAL                                                               as   "PrimaNeta"
        --   , ROUND(PD.VAL_PRIMA_DIFERENCIAL*0.19,2)                                                 as   "IVA"
        --   , PD.VAL_PRIMA_DIFERENCIAL + ROUND(PD.VAL_PRIMA_DIFERENCIAL*0.19,2)                      as   "PrimaTotal"
           , Replace( RUT_PERSONA(PE.NMSEC_DNI),'-','')                                             as   "RUT"
           , PERSONA(PE.NMSEC_DNI)                                                                  as   "Nombre"
           , case when COR.DSNOMBRES = 'DIRECTO' or COR.DSNOMBRES= 'DIRECTO SUCURSAL' then 'AVLA SERVICIOS S.A.'
                  else COR.DSNOMBRES
                  end                                                                               as   "Corredor"                                    
           , case when COR.DSNOMBRES = 'DIRECTO' or COR.DSNOMBRES= 'DIRECTO SUCURSAL' then 12
           else INTER.PORCENTAJE_COMISION end                                                       as   "Comision"
        --   , 100                                                                                    as   "Participacion"
           , nvl(TGE.DSGRUPO_ECONOMICO,'NO TIENE')                                                  as   "Grupo_Economico"    
           , ben.csv                                                                                as   "Beneficiario"
           ,case when COB.DESCRIP_DATO = 'PROMESA DE COMPRAVENTA' 
                then 'ZAROR*****ENRIQUE*' 
                else COMER.DSNOMBRES
                end  as   "Ejecutivo"
           , SUBSTR(PO.COD_TIPO_POLIZA,-3,3)                                                        as   "POL"
        --   , PRY.NOM_PROYECTO                                                                       as   "Proyecto"
        --   , PVER.DSOCUPACION                                                                       as   "Adicional"
           , case 
              when PO.COD_ESTADO = 'AN'                                                            
                then 'Anulada'
              when PO.COD_ESTADO = 'EM'                                                            
                then 'Activa'
              when PO.COD_ESTADO = 'IN'                                                            
                then 'Inactiva'
              when PO.COD_ESTADO = 'PE'                                                            
                then 'Pendiente'
              when PO.COD_ESTADO = 'VE'                                                            
                then 'Vencida'
              when PO.COD_ESTADO = 'VI'                                                            
                then 'Activa'
              when PO.COD_ESTADO = 'SI'                                                            
                then 'Siniestrada'
             end                                                                                    as  "Estado"
           , case 
                    when PD.COD_TIPO_DETALLE = 'AN'                                                            
                      then 'Anulacion'
                    when PD.COD_TIPO_DETALLE = 'EM'                                                            
                      then 'Emision'
                    when PD.COD_TIPO_DETALLE = 'IN'                                                            
                      then 'Inactivacion'
                    when PD.COD_TIPO_DETALLE = 'EN'                                                            
                      then 'Endoso'
                    when PD.COD_TIPO_DETALLE = 'VE'                                                            
                      then 'Vencimiento'
                   end                                                                              as  "Tipo_Operacion"
        --   , case 
        --      when PD.FEC_FINAL < sysdate 
         --       then 'Vencida' 
         --       else 'No'                     
        --     end                                                                                    as  "Vencimiento"
    
    
          , case
              when pd.nmsec_dni_comercial in (274433,249096,265855,242790,240962)
                then 'CORR.INMOBILIARIOS' 
    
              when pd.nmsec_dni_comercial in (201582,241166,234039,281098,198654,315781,272890)
                then 'CORREDORES'
    
              when pd.nmsec_dni_comercial in (274432,234041,274639,272890,220629,268827,266850,204690,283646,283054,220214,236341,223157,272664,281173)
                then 'DIRECTO'
    
              when pd.nmsec_dni_comercial in (267964,267026,206701,229661,208316,205132,206119,247769,275482,283724,284617,302945,301966,316858,247761)
                then 'SUCURSALES'
              when COB.DESCRIP_DATO = 'PROMESA DE COMPRAVENTA' 
                then 'CORR.INMOBILIARIOS' 
            else 'N/A'
            end                                                                                     as  "Canal"
    
    
          , PD.NUM_DIAS                                                                             as  "Dias Duracion"
      --    , case 
      --          when PD.NUM_DIAS < 182 then 'Menor a 6 Meses'
      --          when PD.NUM_DIAS < 365 then '6 - 12 Meses'
      --          when PD.NUM_DIAS < 730 then '12 - 24 Meses'
      --          else 'Mayor a 24 Meses'
      --       end                                                                                    as  "Rango_Plazo"
          , TRUNC(PD.FEC_REGISTRA)                                                                  as  "Dia_de_Emision"
     --     , PD.PORCENTAJE_TASA                                                                      as  "Tasa_norm"
     --     ,ROUND(PD.VAL_ASEGURADO_DIFERENCIAL*PD.PORCENTAJE_TASA,2)                                as  "Factor_precio"
     --     , 0                                                                                       as  "Emision_mensual"     
     --     , case
     --          when ben.csv = 'AVLA S.A.G.R.' or ben.csv = 'FIRST AVAL'
     --            then round(PD.VAL_PRIMA_DIFERENCIAL*2,2)
     --            else PD.VAL_PRIMA_DIFERENCIAL
     --       end                                                                                     as "Prima_Comercial"
    
          , CASE 
                WHEN PO.CDTIPO_MONEDA = '220' THEN ROUND(PD.VAL_PRIMA_DIFERENCIAL*(SELECT TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '220' AND TC.CDTIPO_MONEDA_DESTINO ='108' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
                WHEN PO.CDTIPO_MONEDA = '109' THEN ROUND(PD.VAL_PRIMA_DIFERENCIAL*(SELECT TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '109' AND TC.CDTIPO_MONEDA_DESTINO ='108' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
                WHEN PO.CDTIPO_MONEDA = '110' THEN ROUND(PD.VAL_PRIMA_DIFERENCIAL*(SELECT TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '110' AND TC.CDTIPO_MONEDA_DESTINO ='108' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
                WHEN PO.CDTIPO_MONEDA = '214' THEN ROUND(PD.VAL_PRIMA_DIFERENCIAL*(SELECT 1/TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '108' AND TC.CDTIPO_MONEDA_DESTINO ='214' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
                ELSE PD.VAL_PRIMA_DIFERENCIAL END
                                                                                                    as  "Prima_UF"
                                                                                                    
        , CASE 
                WHEN PO.CDTIPO_MONEDA = '220' THEN ROUND(PD.VAL_ASEGURADO_DIFERENCIAL*(SELECT TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '220' AND TC.CDTIPO_MONEDA_DESTINO ='108' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
                WHEN PO.CDTIPO_MONEDA = '109' THEN ROUND(PD.VAL_ASEGURADO_DIFERENCIAL*(SELECT TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '109' AND TC.CDTIPO_MONEDA_DESTINO ='108' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
                WHEN PO.CDTIPO_MONEDA = '110' THEN ROUND(PD.VAL_ASEGURADO_DIFERENCIAL*(SELECT TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '110' AND TC.CDTIPO_MONEDA_DESTINO ='108' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
                WHEN PO.CDTIPO_MONEDA = '214' THEN ROUND(PD.VAL_ASEGURADO_DIFERENCIAL*(SELECT 1/TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '108' AND TC.CDTIPO_MONEDA_DESTINO ='214' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
                ELSE PD.VAL_ASEGURADO_DIFERENCIAL END
                                                                                                    as  "Capital_UF"
    
    
     --     , case
     --          when ben.csv = 'AVLA S.A.G.R.' or ben.csv = 'FIRST AVAL'  
     --          then 
      --            CASE 
      --             WHEN PO.CDTIPO_MONEDA = '220' THEN ROUND(PD.VAL_PRIMA_DIFERENCIAL*2*(SELECT TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '220' AND TC.CDTIPO_MONEDA_DESTINO ='108' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
      --             WHEN PO.CDTIPO_MONEDA = '109' THEN ROUND(PD.VAL_PRIMA_DIFERENCIAL*2*(SELECT TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '109' AND TC.CDTIPO_MONEDA_DESTINO ='108' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
     --              WHEN PO.CDTIPO_MONEDA = '110' THEN ROUND(PD.VAL_PRIMA_DIFERENCIAL*2*(SELECT TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '110' AND TC.CDTIPO_MONEDA_DESTINO ='108' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
     --              WHEN PO.CDTIPO_MONEDA = '214' THEN ROUND(PD.VAL_PRIMA_DIFERENCIAL*2*(SELECT 1/TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '108' AND TC.CDTIPO_MONEDA_DESTINO ='214' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
     --              ELSE round(PD.VAL_PRIMA_DIFERENCIAL*2,2) END
     --          else
     --             CASE 
     --              WHEN PO.CDTIPO_MONEDA = '220' THEN ROUND(PD.VAL_PRIMA_DIFERENCIAL*(SELECT TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '220' AND TC.CDTIPO_MONEDA_DESTINO ='108' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
     --              WHEN PO.CDTIPO_MONEDA = '109' THEN ROUND(PD.VAL_PRIMA_DIFERENCIAL*(SELECT TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '109' AND TC.CDTIPO_MONEDA_DESTINO ='108' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
     --              WHEN PO.CDTIPO_MONEDA = '110' THEN ROUND(PD.VAL_PRIMA_DIFERENCIAL*(SELECT TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '110' AND TC.CDTIPO_MONEDA_DESTINO ='108' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
     --              WHEN PO.CDTIPO_MONEDA = '214' THEN ROUND(PD.VAL_PRIMA_DIFERENCIAL*(SELECT 1/TC.VAL_FACTOR_CONVERSION FROM TSGE_TIPO_CAMBIO TC WHERE TC.CDTIPO_MONEDA_ORIGEN = '108' AND TC.CDTIPO_MONEDA_DESTINO ='214' AND TC.FEC_TIPO_CAMBIO = to_number(to_char(PD.FEC_REGISTRA, 'yyyyMMdd'))),2)
     --              ELSE PD.VAL_PRIMA_DIFERENCIAL END
    --           end           
           --                                                                         as  "Prima_UF_Comercial"
    
          --, PD.ID_POLIZA || case when nvl(PD.NUM_CORRELATIVO,0) > 0 then '/' || PD.NUM_CORRELATIVO end as "Poliza/Endoso"
      --    , PD.ID_POLIZA || case when nvl(PD.NUM_ENDOSO,0) > 0 then '/' || PD.NUM_ENDOSO end as "Poliza/Endoso"
    --      , SEC.DSNOMBRE_SECTOR
          ,CASE 
             WHEN pd.nmsec_dni_comercial IN (229661,302317,316858) 
              THEN 'Vina del Mar' 
    
           WHEN pd.nmsec_dni_comercial IN (205132,303570) 
              THEN 'Antofagasta' 
    
           WHEN pd.nmsec_dni_comercial IN (206119,247761,317012) 
              THEN 'Concepcion' 
    
           WHEN pd.nmsec_dni_comercial IN (302945,284617) 
              THEN 'Temuco' 
    
           WHEN pd.nmsec_dni_comercial IN (301966,281173) 
             THEN 'Talca' 
    
           WHEN pd.nmsec_dni_comercial IN (283724,206701,316867) 
             THEN 'Puerto Montt' 
    
             ELSE 'Santiago'  
             END                                                                    as "Sucursal"
             
             ,minu.tasa_tarifario tasa_tarifario
          ,  minu.cod_tipo_clasificacion clasificacion
          
          
          
          
          
          , minu.cod_tipo_minuta tipo_minuta
        
          
          , minu.id_minuta minuta
        
          
           , case  when PD.VAL_ASEGURADO_DIFERENCIAL*PD.NUM_DIAS <> 0 then round((PD.VAL_PRIMA_DIFERENCIAL*360)/(PD.VAL_ASEGURADO_DIFERENCIAL*PD.NUM_DIAS)*100,2)
            else PD.PORCENTAJE_TASA end as "Tasa_Real"
         -- , case  when PD.VAL_ASEGURADO_DIFERENCIAL*PD.NUM_DIAS <> 0 then round((PD.VAL_PRIMA_DIFERENCIAL*360)/(PD.VAL_ASEGURADO_DIFERENCIAL*PD.NUM_DIAS)*100,2)
         --   else PD.PORCENTAJE_TASA end as "Tasa_Real"
       --   ,sfr.score_categoria score
      --    ,sfr.score resultado
     --     ,sfr.score_request parametros
    
    --     , PD.ID_POLIZA_DETALLE                                                     as "Poliza_Detalle"
     --    , INTER.CDINTERMEDIARIO                                            as "CDINTERMEDIARIO"
    
          --, case when csol.id_poliza is not null and PD.COD_TIPO_DETALLE = 'EM' then  1 else 0  end
    
      from TSGE_POLIZA_DETALLE            PD
      left join TSGE_POLIZA               PO    on PD.ID_POLIZA           = PO.ID_POLIZA 
      left join TSCR_TIPOS_MONEDAS        MO    on PO.CDTIPO_MONEDA       = MO.CDTIPO_MONEDA
      left join TSCR_PERSONAS             PE    on PO.NMSEC_DNI_TOMADOR   = PE.NMSEC_DNI
      left join TSGE_CATALOGO_GENERAL     COB   on PO.COD_TIPO_COBERTURA  = COB.COD_DATO 
      left join TSGE_CATALOGO_GENERAL     COB2  on PO.COD_TIPO_CANAL      = COB.COD_DATO 
      left join TSGE_POLIZA_INTERMEDIARIO INTER on PD.ID_POLIZA_DETALLE   = INTER.ID_POLIZA_DETALLE
      left join TSCR_INTERMEDIARIOS       COR   on INTER.CDINTERMEDIARIO  = COR.CDINTERMEDIARIO  
      left join TSGE_PROYECTO             PRY   on PO.ID_PROYECTO         = PRY.ID_PROYECTO
      left join TSGE_POLIZA_VERDE         PVER  on PD.ID_POLIZA           = PVER.ID_POLIZA
      left join TCUM_TOMADOR_X_GRUPO      TG    on PO.NMSEC_DNI_TOMADOR   = TG.NMSEC_DNI_TOMADOR
      left join TSCR_GRUPOS_ECONOMI       TGE   on TG.NMSEC_GRUPOS_ECONO  = TGE.NMSEC_GRUPOS_ECONO
      left join TSCR_PERSONAS             COMER on PD.NMSEC_DNI_COMERCIAL = COMER.NMSEC_DNI
      left join TSGE_CLIENTE_SOLICITUD    CSOL  on PD.ID_POLIZA_DETALLE   = CSOL.ID_POLIZA
      left join TCUM_TOMADORES            TOM   on PO.nmsec_dni_tomador   = TOM.nmsec_dni_tomador
      left join TSCR_ACTIVIDADES_ECO      ACT   on TOM.cdactividad        = ACT.cdactividad
      left join TSCR_SECTORES_ECO         SEC   on ACT.NMSEC_SECTOR       = SEC.NMSEC_SECTOR
      left join TSGE_PROPUESTA    MS    on MS.ID_PROPUESTA = PO.ID_PROPUESTA
      left join TSGE_MINUTA               MINU  on MINU.ID_MINUTA = MS.ID_MINUTA
      left join TSGE_SOLICITUD_FIN_REGISTRO SFR on SFR.ID_SOLICITUD = PO.ID_SOLICITUD
      left join (SELECT LISTAGG(NOM_BENEFICIARIO,', ') WITHIN GROUP(ORDER BY NOM_BENEFICIARIO) csv
                      , ID_POLIZA_DETALLE
                   FROM TSGE_POLIZA_BENEFICIARIO
                  group by ID_POLIZA_DETALLE
                 ) ben on PD.ID_POLIZA_DETALLE = ben.ID_POLIZA_DETALLE
     where PD.COD_TIPO_DETALLE = 'EM'
      --and  PD.COD_TIPO_DETALLE <> 'SI'
      and PD.COD_ESTADO = 'A'
    and to_char(PD.FEC_REGISTRA,'yyyymm')>'202004'
      --and PD.ID_POLIZA=3012018081085
      --and to_char(PD.FEC_REGISTRA,'yyyymm')='201907'
      --AND MO.DSTIPO_MONEDA='DOLARES'
      order by PD.FEC_REGISTRA,1,3) q"""
    
    df = pd.read_sql(query, con)
    df = VaciosaNone(df)
    gc = pygsheets.authorize(client_secret='client_secret_216794021399-4p04b5spcl071g07dg7hjl7fqcqhkqfl.apps.googleusercontent.com.json')
    sheet = gc.open_by_key('1bbJ8XDKp9fCNLv5SwXzuDgS7XEbow97_At_0-vK3b6c')
    wks = sheet.worksheet('title', 'Hoja 1')
    wks.clear(start='A1', end=None, fields='userEnteredValue')
    
    
    print("Subiendo Base a gsheets a las: " + str(datetime.datetime.now()))
    wks.set_dataframe(df, start="A1", copy_index=False, copy_head=True, fit=True, escape_formulae=False)
    
    print("Proceso termino sin problemas a las: "+ str(datetime.datetime.now()))