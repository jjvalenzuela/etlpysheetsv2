#Detalle
import cx_Oracle
import pandas as pd
import pygsheets

def VaciosaNone(df):
    # Funcion para buscar nan y nat dentro de un dataframe y convertirlos en vacios
    # Cambia los nan y NaT a Vacios, no sirve el pd.where para cambiar los NaT a Vacios
    df = df.apply(lambda x: x.apply(lambda y: '' if y != y else y), axis=0)
    df = df.where((pd.notnull(df)), '')
    return (df)

con = cx_Oracle.connect('PGC_CL/PGC_CL@//Oracle.avalchile.cl:1521/ORCL')

gc = pygsheets.authorize(
    client_secret='C:/Users/jjvalenzuela/Documents/Python Scripts/git/etlpysheetsv2/client_secret_216794021399-4p04b5spcl071g07dg7hjl7fqcqhkqfl.apps.googleusercontent.com.json')
sheet = gc.open_by_key('1_dN645blosCPrkb-RdmUfBi7ToMxn5P2eGsCeF1P7eU')
wks = sheet.worksheet('title', 'Detalle')

   
#Detalle 
query="""SELECT CASE BO.TIPO_OPERACION
           WHEN 'EM' THEN POL_2.ID_POLIZA
           ELSE POL.ID_POLIZA
           END                                             AS ID_POLIZA,
       NULL                                                AS ESTADO_POLIZA,
       PRO.ID_PROPUESTA,
       NULL                                                AS ESTADO_PROPUESTA,
       TOM.DSNOMBRES                                       AS TOMADOR,
       to_char(BOD.FEC_REGISTRA, 'dd/MM/yyyy HH:MI:SS AM') AS FEC_ENVIO_A_OPERACIONES,
       CASE
           WHEN BOD.COD_ESTADO IN ('T', 'C')
               THEN to_char(BOD.FEC_MODIFICA, 'dd/MM/yyyy HH:MI:SS AM')
           ELSE
               NULL
           END                                             AS FEC_TERMINO_O_CANCELACION,
       NULL                                                AS FEC_INICIO_VIGENCIA,
       NULL                                                AS FEC_FIN_VIGENCIA,
       CASE BO.TIPO_OPERACION
           WHEN 'EM' THEN COB_2.NOM_DATO
           ELSE COB.NOM_DATO
           END                                             AS COBERTURA,
       UPPER(BOD.USU_REGISTRA)                                   AS EJECUTIVO,
       CASE
           WHEN BOD.COD_ESTADO IN ('T', 'C')
               THEN UPPER(BOD.USU_MODIFICA)
           ELSE
               NULL
           END                                             AS GESTOR,
       CASE BO.COD_PRIORIDAD
           WHEN 'A' THEN 'ALTA'
           WHEN 'M' THEN 'MEDIA'
           WHEN 'B' THEN 'BAJA'
           END                                             AS PRIORIDAD,
       TIPO.NOM_DATO                                       AS TIPO_OPERACION,
       CASE BO.TIPO_OPERACION
           WHEN 'EM' THEN BENEF_2.NOM_BENEFICIARIO
           ELSE BENEF.NOM_BENEFICIARIO
           END                                             AS BENEFICIARIO,
       ESTADO.NOM_DATO                                     AS ESTADO_BANDEJA
FROM TSGE_BANDEJA_OPERACION BO
         INNER JOIN TSGE_BANDEJA_OPERACION_DETALLE BOD on BO.ID_BANDEJA = BOD.ID_BANDEJA
         LEFT JOIN TSGE_CATALOGO_GENERAL TIPO
                   ON TIPO.COD_DATO = BO.TIPO_OPERACION AND TIPO.NOM_GRUPO = 'BANDEJA_OPERACION'
         LEFT JOIN TSGE_CATALOGO_GENERAL ESTADO
                   ON ESTADO.COD_DATO = BOD.COD_ESTADO AND ESTADO.NOM_GRUPO = 'BANDEJA_ESTADO'
         LEFT JOIN TSGE_PROPUESTA PRO on BOD.ID_PROPUESTA = PRO.ID_PROPUESTA
         LEFT JOIN TSGE_POLIZA POL on BOD.ID_POLIZA = POL.ID_POLIZA
         LEFT JOIN TSGE_POLIZA POL_2 ON POL_2.ID_PROPUESTA = PRO.ID_PROPUESTA
         LEFT JOIN TSGE_CATALOGO_GENERAL COB
                   on POL.COD_TIPO_COBERTURA = COB.COD_DATO and COB.NOM_GRUPO = 'TIPO_COBERTURA'
         LEFT JOIN TSGE_CATALOGO_GENERAL COB_2
                   on PRO.COD_TIPO_COBERTURA = COB_2.COD_DATO and COB_2.NOM_GRUPO = 'TIPO_COBERTURA'
         LEFT JOIN TSCR_PERSONAS TOM on BOD.NMSEC_DNI_TOMADOR = TOM.NMSEC_DNI
         LEFT JOIN TSGE_POLIZA_DETALLE POL_DET ON POL_DET.ID_POLIZA = POL.ID_POLIZA AND POL_DET.NUM_CORRELATIVO = 0
         LEFT JOIN TSGE_POLIZA_BENEFICIARIO BENEF ON BENEF.ID_POLIZA_DETALLE = POL_DET.ID_POLIZA_DETALLE
         LEFT JOIN TSGE_PROPUESTA_BENEFICIARIO BENEF_2 on BENEF_2.ID_PROPUESTA = PRO.ID_PROPUESTA

where to_char(BOD.FEC_MODIFICA,'yyyy-mm')>='2020-01'         

ORDER BY 1, BOD.FEC_REGISTRA
"""
df = pd.read_sql(query,con)
wks.clear(start='A1', end=None, fields='userEnteredValue')
wks.set_dataframe(df, start="A1", copy_index=False, copy_head=True, fit=True, escape_formulae=False)


wks = sheet.worksheet('title', 'Polizas')
#Polizas
query="""select
id_poliza,
to_char(fec_entrega,'dd/MM/yyyy HH:MI:SS AM')
from tsge_poliza
where to_char(fec_registra,'yyyy-mm')>='2020-01'
and fec_entrega is not null
"""
df = pd.read_sql(query,con)
wks.clear(start='A1', end=None, fields='userEnteredValue')
wks.set_dataframe(df, start="A1", copy_index=False, copy_head=True, fit=True, escape_formulae=False)