def scoreChile():
    import pandas as pd
    import pygsheets
    import cx_Oracle
    import datetime
    from math import ceil
    from time import sleep
    
    print('Cargando score Chile')
    print("Cargando paquetes a las: " + str(datetime.datetime.now()))

    def VaciosaNone(df):
        # Funcion para buscar nan y nat dentro de un dataframe y convertirlos en vacios
        # Cambia los nan y NaT a Vacios, no sirve el pd.where para cambiar los NaT a Vacios
        df = df.apply(lambda x: x.apply(lambda y: '' if y != y else y), axis=0)
        df = df.where((pd.notnull(df)), '')
        return (df)

    def changedtype(df, columns):
        for i in columns:
            df[i] = df[i].astype(str)
            df[i] = df[i].str.replace('.', ',')
        return (df)

    print("Cargando base a las: " + str(datetime.datetime.now()))
    con = cx_Oracle.connect('PGC_CL/PGC_CL@//Oracle.avalchile.cl:1521/ORCL')
    query = """select
    substr(pers.dni,2,length(pers.dni)-1) rut,
    pers.dsnombres tomador,
    case when sol.cdtipo_moneda = 108 then 'UF'
         when sol.cdtipo_moneda = 109 then 'UTM'
         when sol.cdtipo_moneda = 220 then 'USD'
        end as "moneda",
    sol.val_asegurado valor_asegurado,
    sf.id_solicitud solicitud,
    sf.usu_registra ejecutivo,
    sf.fec_registra fecha_registro,
    case when sf.score_categoria = 'R' then 'RECHAZADO'
        when sf.score_categoria = 'A' then 'FLUJO NORMAL'
        when sf.score_categoria = 'V' then 'APROB AUTOMATICA' end resultado,
    case when sf.resultado = 'E' then 'ERROR'
        else 'SCORE'
    end FLUJO,
    score,
    cat.nom_dato cobertura
    from tsge_solicitud_fin_registro sf
    join tsge_solicitud sol on sol.id_solicitud = sf.id_solicitud
    join tscr_personas pers on pers.nmsec_dni = sol.nmsec_dni_tomador
    join tsge_catalogo_general cat on cat.cod_dato = sol.cod_tipo_cobertura
    where score_categoria is not null
    order by 7"""

    df = pd.read_sql(query, con)
    df = VaciosaNone(df)
    gc = pygsheets.authorize(
        client_secret='client_secret_216794021399-4p04b5spcl071g07dg7hjl7fqcqhkqfl.apps.googleusercontent.com.json')
    sheet = gc.open_by_key('1qyWGzpCk_yTiI-6HU5cVxA6JumpSDZh3X6tgwps4z7o')
    wks = sheet.worksheet('title', 'Hoja 1')
    wks.clear(start='A1', end=None, fields='userEnteredValue')
    sleep(120)
    FECHA = []
    RUTT = []
    MONTOUF = []
    CANAL = []
    for i in range(2, len(df) + 2):
        FECHA += ["=IZQUIERDA(G" + str(i) + ";10)"]
        RUTT += ["=CONCATENAR(A" + str(i) + ")"]
        MONTOUF += ["""=REDONDEAR(SI(C%d="UF";D%d;D%d*'Tipo Cambio'!B1/'Tipo Cambio'!B2)/1000;4)""" % (i, i, i)]
        CANAL += ["=BUSCARV(F%d;Canal!A:B;2;FALSO)" % (i)]
    df["FECHA"] = FECHA
    df["RUT T"] = RUTT
    df["MONTO UF"] = MONTOUF
    df["CANAL"] = CANAL
    df = changedtype(df, ["VALOR_ASEGURADO", "SCORE"])
    wks.resize(rows=len(df)+1, cols=df.shape[1])

    print("Subiendo Base a gsheets a las: " + str(datetime.datetime.now()))
    size=3300
    copy_head=True
    start=1
    for i in range(ceil(len(df)/size)):
        print("cargando filas %d a %d" % (i*size , (i+1)*size))
        wks.set_dataframe(df[size*i:(size*(i+1))], start="A"+str(i*size+2-start), copy_index=False, copy_head=copy_head, fit=False, escape_formulae=False)
        copy_head=False
        start = 0
        sleep(75)
    #wks.set_dataframe(df, start="A1", copy_index=False, copy_head=True, fit=True, escape_formulae=False)

    wks = sheet.worksheet('title', 'Fecha carga')
    wks.update_col(1, ["Fecha", str(datetime.datetime.now() - datetime.timedelta(hours=3))])
    print("Proceso termino sin problemas a las: " + str(datetime.datetime.now()))