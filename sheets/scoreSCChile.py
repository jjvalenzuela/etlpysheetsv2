def ScoreSCChile():
    import pandas as pd
    import pygsheets
    import datetime
    from sqlalchemy import create_engine
    
    print('Cargando score Chile')
    print("Cargando paquetes a las: " + str(datetime.datetime.now()))
    
    def VaciosaNone(df):
        # Funcion para buscar nan y nat dentro de un dataframe y convertirlos en vacios
        # Cambia los nan y NaT a Vacios, no sirve el pd.where para cambiar los NaT a Vacios
        df = df.apply(lambda x: x.apply(lambda y: '' if y != y else y), axis=0)
        df = df.where((pd.notnull(df)), '')
        return (df)
    
    def changedtype(df, columns):
        for i in columns:
            df[i] = df[i].astype(str)
            df[i] = df[i].str.replace('.', ',')
        return (df)
    
    print("Cargando base a las: " + str(datetime.datetime.now()))
    con = create_engine('postgres://external_sources_prod_ro_pgalves:Zjh9D7uB48XM8E7Q@api-external-documents-db-repl.avla.com:5432/external_sources_prod')
    
    query = """
    select sq.*,
    	case
    		when ((sq.razones::json->0)->'code')::text like '%%with_protest%%' then 'mantiene protesto'
    		when ((razones::json->0)->'code')::text  like '%%with_business_order_terms%%' then 'mantiene termino de giro'
    		when ((razones::json->0)->'code')::text  like '%%on_marklist%%' then 'en marklist'
    		when ((razones::json->0)->'code')::text  like '%%on_siniester%%' then 'mantiene siniestro'
    		when ((razones::json->0)->'code')::text  like '%%accept_by_model%%' then 'aceptado por modelo'
    		when ((razones::json->0)->'code')::text  like '%%executive_by_model%%' then 'a evaluar por ejecutivo'
    		when ((razones::json->0)->'code')::text  like '%%reject_by_model%%' then 'rechazado por modelo'
    		when ((razones::json->0)->'code')::text  like '%%error_previous%%' then 'errores de tipo de datos'
    		when ((razones::json->0)->'code')::text  like '%%active_prorrogue%%' then 'tiene proroga activa'
    		when ((razones::json->0)->'code')::text  like '%%low_company_time%%' then 'baja antiguedad'
    		when ((razones::json->0)->'code')::text  like '%%bank_default%%' then 'tiene mora'
    		when ((razones::json->0)->'code')::text  like '%%is_identity_lock%%' then 'bloqueado desde artemisa'
    		when ((razones::json->0)->'code')::text  like '%%high_infringement%%' then 'alto nivel de infraciones'
    		when ((razones::json->0)->'code')::text  like '%%penalty_infringement%%' then 'alto nivel de multas'
    		when ((razones::json->0)->'code')::text  like '%%with_default%%' then 'alto nivel de morosidad'
    		when ((razones::json->0)->'code')::text  like '%%error_sbif%%' then 'error sbif'
    		when ((razones::json->0)->'code')::text  like '%%error_equifax%%' then 'error equifax'
    		when ((razones::json->0)->'code')::text  like '%%on_economic_group%%' then 'analista por grupo económico'
    		when ((razones::json->0)->'code')::text  like '%%max_amount%%'  then 'analista por monto máximo'
        end as descripcionResolucion
       from (
    select
    	document_id as id,
    	de.creation_datetime as fe_creacion,
    	de.input_parameters::json->'input' as inputs,
    	(de.input_parameters::json->'input')::json->'line_id'  as id_linea,
    	(de.input_parameters::json->'input')::json->'inicio_actividades'  as inicio_actividades,
    	(de.input_parameters::json->'input')::json->'previous_canceled'  as cancelaciones_anteriores,
    	(de.input_parameters::json->'input')::json->'deuda_morosa_30_90'  as deuda_morosa_30_90,
    	(de.input_parameters::json->'input')::json->'deuda_direct_vig'  as deuda_direct_vig,
    	(de.input_parameters::json->'input')::json->'cantidadprorrogas'  as cantidadprorrogas,
    	(de.input_parameters::json->'input')::json->'Monto_Mora_UF'  as Monto_Mora_UF,
    	(de.input_parameters::json->'input')::json->'deuda_leasing'  as deuda_leasing,
    	(de.input_parameters::json->'input')::json->'Monto_Protesto_Total_UF'  as Monto_Protesto_Total_UF,
    	(de.input_parameters::json->'input')::json->'deuda_dir_venc_180d_3years'  as deuda_dir_venc_180d_3years,
    	(de.input_parameters::json->'input')::json->'deuda_direct_venc'  as deuda_direct_venc,
    	(de.input_parameters::json->'input')::json->'deuda_indirec_vig'  as deuda_indirec_vig,
    	(de.input_parameters::json->'input')::json->'deuda_morosa_leasing'  as deuda_morosa_leasing,
    	(de.input_parameters::json->'input')::json->'N_Infracciones_Previsionales'  as N_Infracciones_Previsionales,
    	(de.input_parameters::json->'input')::json->'deuda_indirec_vencida'  as deuda_indirec_vencida,
    	(de.input_parameters::json->'input')::json->'termino_giro'  as termino_giro,
    	(de.input_parameters::json->'input')::json->'N_Protestos'  as N_Protestos,
    	(de.input_parameters::json->'input')::json->'g_economico'  as g_economico,
    	(de.input_parameters::json->'input')::json->'previous_approved'  as Aprobaciones_anteriores,
    	(de.input_parameters::json->'input')::json->'mto_linea_disponible'  as mto_linea_disponible,
    	(de.input_parameters::json->'input')::json->'bo_is_nominated'  as bo_is_nominated,
    	(de.input_parameters::json->'input')::json->'Bancarizado'  as Bancarizado,
    	(de.input_parameters::json->'input')::json->'is_it_previously_sinister'  as registra_siniestros,
    	(de.input_parameters::json->'input')::json->'on_marklist'  as registra_marklist,
    	(de.input_parameters::json->'input')::json->'is_it_active_sale_extension'  as is_it_active_sale_extension,
    	(de.input_parameters::json->'input')::json->'identity_risk_clasif'  as identity_risk_clasif,
    	(de.input_parameters::json->'input')::json->'is_identity_lock'  as is_identity_lock,
    	(de.input_parameters::json->'input')::json->'rut'  as rut,
    	(de.input_parameters::json->'input')::json->'nm_requested_amount'  as monto_solicitado_UF,
    	(de.json_data::json->'response')::json->'score' as score,
    	(de.json_data::json->'response')::json->'reason' as razones,
    	(de.json_data::json->'response')::json->'is_it_automatic_resolution' as resolucion_automatica,
    	(de.json_data::json->'response')::json->'external_message' as mensaje_externo,
    	(de.json_data::json->'response')::json->'resolution' as resolucion_recomendada,
    	case
    		when ((de.json_data::json->'response')::json->'reason')::text LIKE '%%max_amount%%' THEN 1
    		else 0
    	end AS monto_superior_tope,
        case
        when ((de.json_data::json->'response')::json->'reason')::text LIKE '%%classification_to_analyst%%' THEN 1
        else 0
        end AS salto_politica_clasificación_de_riesgo
    from document_entity de
    where de.source_id in (
    	select source_id
    	from
    		source_entity se2
    	where se2."name" = 'ClScModeloPredictivoV4')) sq;"""
    
    df = pd.read_sql(query, con)
    df = VaciosaNone(df)
    nombre_deudor = []
    dni_deudor = []
    nm_policy_number = []
    nombre_asegurado = []
    Resolucion = []
    for i in range(2, len(df) + 2):
        nombre_deudor += ["=BUSCARV(D" + str(i) + ",'Líneas'!A:E,5,FALSO)"]
        dni_deudor += ["=BUSCARV(D" + str(i) + ",'Líneas'!A:J,4,FALSO)"]
        nm_policy_number += ["=BUSCARV(D" + str(i) + ",'Líneas'!A:J,6,FALSO)"]
        nombre_asegurado += ["=BUSCARV(D" + str(i) + ",'Líneas'!A:J,8,FALSO)"]
        Resolucion += ['=SI(AL%d=1,"Excluido por monto",SI(AI%d=FALSO(),"Resolución manual",SI(Y(AK%d="R",AN%d<>"rechazado por modelo"),"Rechazo política",SI(AK%d="","Flujo normal",SI(AN%d="rechazado por modelo","Rechazo score",SI(AK%d="A","Aprobado score",0))))))' % (i,i,i,i,i,i,i)]
    
    df["nombre_deudor"] = nombre_deudor
    df["dni_deudor"] = dni_deudor
    df["nm_policy_number"] = nm_policy_number
    df["nombre_asegurado"] = nombre_asegurado
    df["Resolucion"] = Resolucion
    
    
    
    gc = pygsheets.authorize(
        client_secret='client_secret_216794021399-4p04b5spcl071g07dg7hjl7fqcqhkqfl.apps.googleusercontent.com.json')
    sheet = gc.open_by_key('1BlEhMILA7RrEaXz98Q1zRRueFwfemfSOFPU2IZvqdcE')
    wks = sheet.worksheet('title', 'Hoja 1')
    wks.clear(start='A1', end=None, fields='userEnteredValue')
    
    print("Subiendo Base a gsheets a las: " + str(datetime.datetime.now()))
    wks.set_dataframe(df, start="A1", copy_index=False, copy_head=True, fit=True, escape_formulae=False)
    
    
    print('Subiendo lineas')
    con = create_engine('postgres://seguro_credito_prod_equipo_pia_ro:seguro_credito_prod_equipo_pia_ro_DZA6p5@back-seguro-credito-prod-db-replica.avla.com:5432/seguro_credito_prod')
    query = """SELECT
    	l.nm_line_id,
    	l.ts_creation,
    	l.nm_requested_amount,
    	debtor.va_identity_field as dni_deudor,
    	debtor.va_complete_name as nombre_deudor,
    	p.nm_policy_number,
    	insured.va_identity_field as dni_asegurado,
    	insured.va_complete_name as nombre_asegurado,
    	pt.name as producto,
    	case when l.va_state_tags_concatenated like '%%automaticResolutionOverriden%%' then 1 else 0
    	end AS artemisa_ignora_modelo
    from line l
    	left join person_identity debtor on l.nm_person_identity_id = debtor.nm_person_identity_id
    	left join "policy" p on l.nm_policy_id = p.nm_policy_id
    	left join product pr on p.nm_product_id =pr.nm_product_id
    	left join product_type pt on pr.nm_product_type_id = pt.nm_product_type_id
    	left join rel_person_identity_policy rel on rel.nm_policy_id = p.nm_policy_id
    	left join person_identity insured on insured.nm_person_identity_id = rel.nm_person_identity_id
    --where l.nm_line_id = 499370 and rel.nm_relation_type = 'I'
    where rel.nm_relation_type = 'I'
    and l.ts_creation >'2020-10-13'
    order by 2;"""
    
    wks = sheet.worksheet('title', 'Líneas')
    wks.clear(start='A1', end=None, fields='userEnteredValue')
    df = pd.read_sql(query, con)
    df = VaciosaNone(df)
    nm_requested_amount_uf = []
    for i in range(2, len(df) + 2):
        nm_requested_amount_uf += ["""=SI(DERECHA(I%d,2)="UF",C%d,REDONDEAR(C%d*tc!$B$3/tc!$B$2,0))""" % (i, i, i)]
    df["nm_requested_amount_uf"]=nm_requested_amount_uf
    
    print("Subiendo Base a gsheets a las: " + str(datetime.datetime.now()))
    wks.set_dataframe(df, start="A1", copy_index=False, copy_head=True, fit=True, escape_formulae=False)
    
    



