def scorePeru():
    import pandas as pd
    import pygsheets
    import cx_Oracle
    import datetime

    print('Cargando score Peru')
    print("Cargando paquetes a las: " + str(datetime.datetime.now()))

    def VaciosaNone(df):
        # Funcion para buscar nan y nat dentro de un dataframe y convertirlos en vacios
        # Cambia los nan y NaT a Vacios, no sirve el pd.where para cambiar los NaT a Vacios
        df = df.apply(lambda x: x.apply(lambda y: '' if y != y else y), axis=0)
        df = df.where((pd.notnull(df)), '')
        return (df)

    def changedtype(df, columns):
        for i in columns:
            df[i] = df[i].astype(str)
            df[i] = df[i].str.replace('.', ',')
        return (df)


    con = cx_Oracle.connect('USU_CON/USU_CON@//avladb-peru.cxrevrkr3ozw.us-east-1.rds.amazonaws.com:1521/ORCL')
    query = """
    select distinct
    m.id_minuta minuta,
    CASE WHEN m.COD_ESTADO = 'PR' THEN 'PreRegistrada'
         WHEN m.COD_ESTADO = 'RE' THEN 'Registrada'
         WHEN m.COD_ESTADO = 'PE' THEN 'Pendiente de Aprobacion'
         WHEN m.COD_ESTADO = 'AP' THEN 'Aprobada'
         WHEN m.COD_ESTADO = 'RC' THEN 'Rechazada'
         WHEN m.COD_ESTADO = 'OB' THEN 'Observada'
         WHEN m.COD_ESTADO = 'CE' THEN 'Cerrada'
         WHEN m.COD_ESTADO = 'IN' THEN 'Inactiva'
         ELSE ''
    END AS "ESTADO MINUTA",
    s.id_solicitud,
    CASE WHEN S.COD_ESTADO = 'AP' THEN 'Aprobada'
         WHEN S.COD_ESTADO = 'RE' THEN 'Registrada'
         WHEN S.COD_ESTADO = 'PE' THEN 'Pendiente de Aprobacion'
         WHEN S.COD_ESTADO = 'RC' THEN 'Rechazada'
         WHEN S.COD_ESTADO = 'IN' THEN 'Inactiva'
         WHEN S.COD_ESTADO = 'CP' THEN 'Con Propuesta'
        WHEN S.COD_ESTADO = 'RF' THEN 'Con registro finalizado'
         ELSE ''
    END AS "ESTADO SOLICITUD",
    substr(pers.dni,2,length(pers.dni)-1) ruc,
    pers.dsnombres tomador,
    cat.nom_dato cobertura,
    case when ds.score = 1 then 'RECHAZADO'
        when ds.score = 0.5 then 'FLUJO NORMAL'
        when ds.score = 0 then 'APROB AUTOMATICA' end resultado,
    case when m.cdtipo_moneda = 108 then 'S/'
         when m.cdtipo_moneda = 220 then 'USD'
        end as "moneda",
    s.val_asegurado valor_asegurado,
    ds.usu_registra ejecutivo,
    ds.fec_registra fecha_registro
    --case when ds.score = 'E' then 'ERROR'
    --   else 'SCORE'
    --end FLUJO
    from PGC_PE.tsge_documento_scoring ds
    left join PGC_PE.tsge_minuta m on m.id_minuta = ds.id_documento
    left join PGC_PE.tscr_personas pers on pers.nmsec_dni = m.nmsec_dni_tomador
    left join PGC_PE.tsge_minuta_solicitud ms on ms.id_minuta = m.id_minuta
    left join PGC_PE.tsge_solicitud s on s.id_solicitud = ms.id_solicitud
    left join PGC_PE.tsge_catalogo_general cat on cat.cod_dato = s.cod_tipo_cobertura
    --where score_categoria is not null
    order by 10,1
    """
    print("Cargando base a las: " + str(datetime.datetime.now()))
    df = pd.read_sql(query, con)
    df = VaciosaNone(df)
    gc = pygsheets.authorize(
            client_secret='client_secret_216794021399-4p04b5spcl071g07dg7hjl7fqcqhkqfl.apps.googleusercontent.com.json')
    sheet = gc.open_by_key('1xxRFglFbQKNUgMxW-KsMY9QS9pGwHBFFK6nnP4lIvKc')
    wks = sheet.worksheet('title', 'Sheet1')
    wks.clear(start='A1', end=None, fields='userEnteredValue')
    #df = changedtype(df, ["VALOR_ASEGURADO"])
    df["FECHA_REPORTE"] = datetime.datetime(datetime.datetime.now().year,
                                            datetime.datetime.now().month,
                                            datetime.datetime.now().day)
    print("Subiendo Base a gsheets a las: " + str(datetime.datetime.now()))
    wks.set_dataframe(df, start="A1", copy_index=False, copy_head=True, fit=True, escape_formulae=False)
    print("Proceso termino sin problemas a las: " + str(datetime.datetime.now()))