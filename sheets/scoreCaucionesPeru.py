
def scoreCaucionesPeru():
    import pandas as pd
    import pygsheets
    import cx_Oracle
    import datetime

    def VaciosaNone(df):
        # Funcion para buscar nan y nat dentro de un dataframe y convertirlos en vacios
        # Cambia los nan y NaT a Vacios, no sirve el pd.where para cambiar los NaT a Vacios
        df = df.apply(lambda x: x.apply(lambda y: '' if y != y else y), axis=0)
        df = df.where((pd.notnull(df)), '')
        return (df)
    
    print('Cargando score Peru')
    print("Cargando paquetes a las: " + str(datetime.datetime.now()))

    con = cx_Oracle.connect('USU_CON/USU_CON@//avladb-peru.cxrevrkr3ozw.us-east-1.rds.amazonaws.com:1521/ORCL')
    query = """WITH SCORING AS (
    SELECT
    ROW_NUMBER() OVER (PARTITION BY tm.ID_MINUTA, ts.ID_SOLICITUD ORDER BY tds.FEC_REGISTRA DESC) rn,
    tm.ID_MINUTA minuta,
    CASE
    WHEN TC.NMNIT_CONSORCIO IS NOT NULL THEN substr(TC.NMNIT_CONSORCIO,2,length(TC.NMNIT_CONSORCIO)-1)
    ELSE substr(pers.dni,2,length(pers.dni)-1)
    END ruc,
    CASE
    WHEN tc.DSCONSORCIO IS NOT NULL THEN tc.DSCONSORCIO
    ELSE pers.dsnombres
    END tomador_consorcio,
    ts.ID_SOLICITUD solicitud,
    case when ts.CDTIPO_MONEDA = 108 then 'PEN'
         when ts.CDTIPO_MONEDA = 220 then 'USD'
    end moneda,
    ts.VAL_ASEGURADO valor_asegurado,
    regexp_substr(LOWER(comers.DSEMAIL),q'[(.*)@]',1,1,null,1) ejecutivo,
    tds.FEC_REGISTRA fecha_registro,
    case when REPLACE( REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'[is_it_automatic_resolution":(.*),"external_message]', 1, 1, NULL, 1 ), '\\', '\' ) = 'true' then 'AUTOMATICO'
         when REPLACE( REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'[is_it_automatic_resolution":(.*),"external_message]', 1, 1, NULL, 1 ), '\\', '\' ) = 'false' then 'MANUAL'
         end resolucion,
    tds.SCORE score,
    case
        /* division por monto */
        when replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":null,]',1,1,null,1),'"','') = 'max_amount' then 'FLUJO NORMAL-EXCEDE MONTO'
        /* rechazo politicas */
        when
            replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"Rechazo:]',1,1,null,1),'"','') = 'RNP_disabled' or
            replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"Rechazo:]',1,1,null,1),'"','') = 'bad_califications' or
            replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"Rechazo:]',1,1,null,1),'"','') = 'high_coercive_debt' or
            replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"]',1,1,null,1),'"','') = 'RNP_disabled' or
            replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"]',1,1,null,1),'"','') = 'bad_califications' or
            replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"]',1,1,null,1),'"','') = 'high_coercive_debt'
        then 'RECHAZO-POLITICA'
        /* division resolucion manual score */
        when replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":null,]',1,1,null,1),'"','') = 'error_previus' then 'ERROR'
        when replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":null,]',1,1,null,1),'"','') = 'no_public_tender' then 'NO ES LIC PUBLICA'
        /* division score */
        when tds.score_categoria = 'A' then 'APROBADO'
        when replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":(.*),"resolution"]',1,1,null,1),'"','') = '[]' then 'FLUJO NORMAL-SCORE'
        when tds.score_categoria = 'R' then 'RECHAZO-SCORE'
        /* esta condicion solo deberia ocurrir cuando existe mas de un rechazo y por ende debe ser rechazo politica*/
        else 'RECHAZO-POLITICA'
    end resultado,
    cat.nom_dato cobertura,
    case when tds.score_categoria = 'R' then
        case when replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"Rechazo:]',1,1,null,1),'"','') is null
            then 'Bad_califications'
            else replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"Rechazo:]',1,1,null,1),'"','')
            end
    else null end motivo_rechazo,
    case when ts.CDTIPO_MONEDA = 108 then ts.VAL_ASEGURADO
         when ts.CDTIPO_MONEDA = 220 then round(ts.VAL_ASEGURADO/3.597,2)
    end monto_soles,
    to_char(tds.FEC_REGISTRA,'DD-MM-YYYY') fecha_sol,
    tds.SCORE_RESPONSE,
    tds.score_categoria,
    SCORE_ETIQUETA 
    FROM PGC_PE.TSGE_DOCUMENTO_SCORING tds
    JOIN PGC_PE.TSGE_MINUTA tm ON tds.ID_DOCUMENTO = tm.ID_MINUTA
    JOIN PGC_PE.TSGE_MINUTA_SOLICITUD tms ON tms.id_minuta = tm.id_minuta
    JOIN PGC_PE.TSGE_SOLICITUD ts ON ts.ID_SOLICITUD = tms.ID_SOLICITUD
    LEFT join PGC_PE.tscr_personas pers on pers.nmsec_dni = ts.nmsec_dni_tomador
    LEFT join PGC_PE.TCUM_CONSORCIOS tc on tc.NMCONSORCIO = ts.NMCONSORCIO
    LEFT join PGC_PE.tscr_personas comers on comers.nmsec_dni = ts.NMSEC_DNI_EMPLEADO
    join PGC_PE.tsge_catalogo_general cat on cat.cod_dato = ts.cod_tipo_cobertura AND cat.NOM_GRUPO = 'TIPO_COBERTURA'
    WHERE tds.FEC_REGISTRA >= to_date('13-10-2020','DD-MM-YYYY')
    order BY tds.FEC_REGISTRA DESC
    )
    SELECT MINUTA as Minuta,
        RUC as Ruc,
        TOMADOR_CONSORCIO as "Tomador/Consorcio",
        SOLICITUD as Solicitud,
        MONEDA as Moneda ,
        VALOR_ASEGURADO as "Monto Asegurado",
        EJECUTIVO as Ejecutivo,
        FECHA_REGISTRO as "Fecha Registro" ,
        RESOLUCION as Resolucion,
        RESULTADO as Resultado,
        SCORE as Score ,
        COBERTURA as Cobertura ,
        MOTIVO_RECHAZO as "Motivo Rechazo",
        MONTO_SOLES as "Monto Soles",
        FECHA_SOL as "Fecha corta",
        SCORE_RESPONSE as Score_response,
        SCORE_CATEGORIA as score_categoria,
        SCORE_ETIQUETA  as score_etiqueta,
        case 
            when SCORE_ETIQUETA = 'EE' then 'Error de lectura'
            when SCORE_ETIQUETA = 'MSL' then 'Monto superior al limite'
            when SCORE_ETIQUETA = 'NLP' then 'No es Licitacion Publica'
            when SCORE_ETIQUETA = 'SA' then 'Score Analista'
            when SCORE_ETIQUETA = 'RP' then 'Rechazo por Politicas'
            when SCORE_ETIQUETA = 'RS' then 'Rechazo por Score'
            when SCORE_ETIQUETA = 'AS' then 'Aprobado por Score'
            when SCORE_ETIQUETA = 'CE' then 'Caso extrano'
            else 'Revisar Etiqueta'
        end as Etiqueta
    FROM PGC_PE.SCORING sc
    WHERE sc.rn = 1"""
    
    df = pd.read_sql(query, con)
    df = VaciosaNone(df)
    gc = pygsheets.authorize(
        client_secret='client_secret_216794021399-4p04b5spcl071g07dg7hjl7fqcqhkqfl.apps.googleusercontent.com.json')
    sheet = gc.open_by_key('1uMAC0XtLX4xu-c1w562wVoJYKOcaoQwBt7nbCLiqjK8')
    wks = sheet.worksheet('title', 'bd')
    wks.clear(start='A1', end=None, fields='userEnteredValue')
    
    print("Subiendo Base a gsheets a las: " + str(datetime.datetime.now()))
    wks.set_dataframe(df, start="A1", copy_index=False, copy_head=True, fit=True, escape_formulae=False)
    
    
    
    ###############################################################################
    #agrupar por minuta
    ###############################################################################
    
    print("Subiendo Base minutas a gsheets a las: " + str(datetime.datetime.now()))
    query = """WITH SCORING AS (
    SELECT
    ROW_NUMBER() OVER (PARTITION BY tm.ID_MINUTA, ts.ID_SOLICITUD ORDER BY tds.FEC_REGISTRA DESC) rn,
    tm.ID_MINUTA minuta,
    CASE
    WHEN TC.NMNIT_CONSORCIO IS NOT NULL THEN substr(TC.NMNIT_CONSORCIO,2,length(TC.NMNIT_CONSORCIO)-1)
    ELSE substr(pers.dni,2,length(pers.dni)-1)
    END ruc,
    CASE
    WHEN tc.DSCONSORCIO IS NOT NULL THEN tc.DSCONSORCIO
    ELSE pers.dsnombres
    END tomador_consorcio,
    ts.ID_SOLICITUD solicitud,
    case when ts.CDTIPO_MONEDA = 108 then 'PEN'
         when ts.CDTIPO_MONEDA = 220 then 'USD'
    end moneda,
    ts.VAL_ASEGURADO valor_asegurado,
    regexp_substr(LOWER(comers.DSEMAIL),q'[(.*)@]',1,1,null,1) ejecutivo,
    tds.FEC_REGISTRA fecha_registro,
    case when REPLACE( REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'[is_it_automatic_resolution":(.*),"external_message]', 1, 1, NULL, 1 ), '\\', '\' ) = 'true' then 'AUTOMATICO'
         when REPLACE( REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'[is_it_automatic_resolution":(.*),"external_message]', 1, 1, NULL, 1 ), '\\', '\' ) = 'false' then 'MANUAL'
         end resolucion,
    tds.SCORE score,
    case
        /* division por monto */
        when replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":null,]',1,1,null,1),'"','') = 'max_amount' then 'FLUJO NORMAL-EXCEDE MONTO'
        /* rechazo politicas */
        when
            replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"Rechazo:]',1,1,null,1),'"','') = 'RNP_disabled' or
            replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"Rechazo:]',1,1,null,1),'"','') = 'bad_califications' or
            replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"Rechazo:]',1,1,null,1),'"','') = 'high_coercive_debt' or
            replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"]',1,1,null,1),'"','') = 'RNP_disabled' or
            replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"]',1,1,null,1),'"','') = 'bad_califications' or
            replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"]',1,1,null,1),'"','') = 'high_coercive_debt'
        then 'RECHAZO-POLITICA'
        /* division resolucion manual score */
        when replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":null,]',1,1,null,1),'"','') = 'error_previus' then 'ERROR'
        when replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":null,]',1,1,null,1),'"','') = 'no_public_tender' then 'NO ES LIC PUBLICA'
        /* division score */
        when tds.score_categoria = 'A' then 'APROBADO'
        when replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":(.*),"resolution"]',1,1,null,1),'"','') = '[]' then 'FLUJO NORMAL-SCORE'
        when tds.score_categoria = 'R' then 'RECHAZO-SCORE'
        /* esta condicion solo deberia ocurrir cuando existe mas de un rechazo y por ende debe ser rechazo politica*/
        else 'RECHAZO-POLITICA'
    end resultado,
    cat.nom_dato cobertura,
    case when tds.score_categoria = 'R' then
        case when replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"Rechazo:]',1,1,null,1),'"','') is null
            then 'Bad_califications'
            else replace(REGEXP_SUBSTR( tds.SCORE_RESPONSE, q'["reason":\[{"code":(.*),"external_message":"Rechazo:]',1,1,null,1),'"','')
            end
    else null end motivo_rechazo,
    case when ts.CDTIPO_MONEDA = 108 then ts.VAL_ASEGURADO
         when ts.CDTIPO_MONEDA = 220 then round(ts.VAL_ASEGURADO/3.597,2)
    end monto_soles,
    to_char(tds.FEC_REGISTRA,'MM-DD-YYYY') fecha_sol,
    tds.SCORE_RESPONSE,
    tds.score_categoria,
    SCORE_ETIQUETA,
        case 
            when SCORE_ETIQUETA = 'EE' then 'Error de lectura'
            when SCORE_ETIQUETA = 'MSL' then 'Monto superior al limite'
            when SCORE_ETIQUETA = 'NLP' then 'No es Licitacion Publica'
            when SCORE_ETIQUETA = 'SA' then 'Score Analista'
            when SCORE_ETIQUETA = 'RP' then 'Rechazo por Politicas'
            when SCORE_ETIQUETA = 'RS' then 'Rechazo por Score'
            when SCORE_ETIQUETA = 'AS' then 'Aprobado por Score'
            when SCORE_ETIQUETA = 'CE' then 'Caso extrano'
            else 'Revisar Etiqueta'
        end as Etiqueta 
    FROM PGC_PE.TSGE_DOCUMENTO_SCORING tds
    JOIN PGC_PE.TSGE_MINUTA tm ON tds.ID_DOCUMENTO = tm.ID_MINUTA
    JOIN PGC_PE.TSGE_MINUTA_SOLICITUD tms ON tms.id_minuta = tm.id_minuta
    JOIN PGC_PE.TSGE_SOLICITUD ts ON ts.ID_SOLICITUD = tms.ID_SOLICITUD
    LEFT join PGC_PE.tscr_personas pers on pers.nmsec_dni = ts.nmsec_dni_tomador
    LEFT join PGC_PE.TCUM_CONSORCIOS tc on tc.NMCONSORCIO = ts.NMCONSORCIO
    LEFT join PGC_PE.tscr_personas comers on comers.nmsec_dni = ts.NMSEC_DNI_EMPLEADO
    join PGC_PE.tsge_catalogo_general cat on cat.cod_dato = ts.cod_tipo_cobertura AND cat.NOM_GRUPO = 'TIPO_COBERTURA'
    WHERE tds.FEC_REGISTRA >= to_date('13-10-2020','DD-MM-YYYY')
    order BY tds.FEC_REGISTRA DESC
    )
    SELECT MINUTA as Minuta,
        RUC as Ruc,
        TOMADOR_CONSORCIO as "Tomador/Consorcio",
        /*SOLICITUD as Solicitud,*/
        MONEDA as Moneda ,
        sum(VALOR_ASEGURADO) as "Monto Asegurado",
        /*EJECUTIVO as Ejecutivo,*/
        /*FECHA_REGISTRO as "Fecha Registro" ,*/
        RESOLUCION as Resolucion,
        RESULTADO as Resultado,
        SCORE as Score ,
        /*COBERTURA as Cobertura ,*/
        MOTIVO_RECHAZO as "Motivo Rechazo",
        sum(MONTO_SOLES) as "Monto Soles",
        FECHA_SOL as "Fecha corta",
        SCORE_RESPONSE as Score_response,
        SCORE_CATEGORIA as score_categoria,
        SCORE_ETIQUETA  as score_etiqueta,
        Etiqueta
    FROM PGC_PE.SCORING sc
    WHERE sc.rn = 1
    group by MINUTA,
        RUC,
        TOMADOR_CONSORCIO,
        MONEDA,
        RESOLUCION,
        RESULTADO,
        SCORE,
        MOTIVO_RECHAZO,
        FECHA_SOL,
        SCORE_RESPONSE,
        SCORE_CATEGORIA,
        SCORE_ETIQUETA,
        Etiqueta
    """
    
    df = pd.read_sql(query, con)
    df = VaciosaNone(df)
    gc = pygsheets.authorize(
        client_secret='client_secret_216794021399-4p04b5spcl071g07dg7hjl7fqcqhkqfl.apps.googleusercontent.com.json')
    sheet = gc.open_by_key('1uMAC0XtLX4xu-c1w562wVoJYKOcaoQwBt7nbCLiqjK8')
    wks = sheet.worksheet('title', 'minuta')
    wks.clear(start='A1', end=None, fields='userEnteredValue')
    
    print("Subiendo Base a gsheets a las: " + str(datetime.datetime.now()))
    wks.set_dataframe(df, start="A1", copy_index=False, copy_head=True, fit=True, escape_formulae=False)
    
    print("Proceso termino sin problemas a las: " + str(datetime.datetime.now()))