from python:3.8-slim

RUN apt-get update
#RUN yum -y install python3 && yum -y install python3-pip && yum -y install git

WORKDIR /app
COPY . /app

RUN pip install --upgrade -r requirements.txt

RUN apt-get install -y wget
RUN apt-get install unzip
RUN apt-get install libaio1
RUN wget https://download.oracle.com/otn_software/linux/instantclient/instantclient-basiclite-linuxx64.zip && \
    unzip instantclient-basiclite-linuxx64.zip && rm -f instantclient-basiclite-linuxx64.zip && \
    cd /app/instantclient* && rm -f *jdbc* *occi* *mysql* *README *jar uidrvci genezi adrci && \
    echo /app/instantclient* > /etc/ld.so.conf.d/oracle-instantclient.conf && ldconfig


CMD ["python3", "/app/ETLGsheets.py"]