from sheets import scoreChile
from sheets import scorePeru
from sheets import Tarificador
from sheets import scoreSCChile
from sheets import scoreCaucionesPeru

try:
    scoreChile.scoreChile()
except Exception as e:
    print("no pudo correr scoreChile")
    print(e)
try:
    scorePeru.scorePeru()
except Exception as e:
    print("no pudo correr scorePeru")
    print(e)
try:
    Tarificador.Tarificador()
except Exception as e:
    print("no pudo correr Tarificador")
    print(e)
try:
    scoreSCChile.ScoreSCChile()
except Exception as e:
    print("no pudo correr ScoreSCChile")
    print(e)
try:
    scoreCaucionesPeru.scoreCaucionesPeru()
except Exception as e:
    print("no pudo correr scoreCaucionesPeru")
    print(e)